package com.iitb.mobileict.lokavidya.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Toast;

import com.iitb.mobileict.lokavidya.Communication.ConnectionChecker;
import com.iitb.mobileict.lokavidya.Projectfile;
import com.iitb.mobileict.lokavidya.R;
import com.iitb.mobileict.lokavidya.ui.fragments.browsing.BrowsingVideosFragment;
import com.iitb.mobileict.lokavidya.ui.fragments.creating.CreatingVideosFragment;
import com.iitb.mobileict.lokavidya.ui.fragments.dashboard.DashboardFragment;
import com.iitb.mobileict.lokavidya.ui.fragments.helpfeedback.HelpFeedbackFragment;
import com.iitb.mobileict.lokavidya.ui.fragments.settings.SettingsFragment;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 This class handles the complete set of functionality related to the Navigation Drawer.
 **/

public class NavigationDrawer extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //Handle all the variable declarations here
    FragmentManager fragmentManager;
    Boolean backPress;
    NavigationView navigationView;
    Toolbar toolbar;
    DrawerLayout drawer;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_drawer);
        toolbar = (Toolbar) findViewById(R.id.toolbar_navigation_drawer);
        setSupportActionBar(toolbar);

        sharedpreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = sharedpreferences.edit();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);

        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame_navigation_items, new DashboardFragment()).commit();
    }

    @Override
    public void onBackPressed() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_privacy) {
            return true;
        } else if (id == R.id.action_signout){
            //TODO handle code for Signout Mechanism

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        backPress = false;
        int id = item.getItemId();

        if (id == R.id.nav_dashboard) {
            //This will toggle the dashboard fragment
            navigationView.getMenu().getItem(0).setChecked(true);
            fragmentManager.beginTransaction().replace(R.id.content_frame_navigation_items, new DashboardFragment()).commit();
        } else if (id == R.id.nav_viewing) {
            if(isNetworkAvailable(getApplicationContext())) {
               // if(ConnectionChecker.IsReachable(getApplicationContext())) {
                    Intent viewVid = new Intent(this, BrowseAndViewVideos.class);
                    startActivity(viewVid);
               // }
              //  else
              //  {
              //      Toast.makeText(NavigationDrawer.this, "Server Error", Toast.LENGTH_SHORT).show();
               // }
            }
            else{
                Toast.makeText(this,"Please connect to internet",Toast.LENGTH_SHORT).show();
            }
          //  break;
            //Handle the viewing mechanism
          //  navigationView.getMenu().getItem(1).setChecked(true);
           // fragmentManager.beginTransaction().replace(R.id.content_frame_navigation_items, new BrowsingVideosFragment()).commit();
        } else if (id == R.id.nav_creating) {
            //Handle the creation mechanism
            navigationView.getMenu().getItem(2).setChecked(true);
            fragmentManager.beginTransaction().replace(R.id.content_frame_navigation_items, new CreatingVideosFragment()).commit();
        } else if (id == R.id.nav_settings) {
            //Handle the settings mechanism
            navigationView.getMenu().getItem(3).setChecked(true);
            fragmentManager.beginTransaction().replace(R.id.content_frame_navigation_items, new SettingsFragment()).commit();
        } else if (id == R.id.nav_help_feedback) {
            //Handle the help & feedback mechanism
            navigationView.getMenu().getItem(4).setChecked(true);
            fragmentManager.beginTransaction().replace(R.id.content_frame_navigation_items, new HelpFeedbackFragment()).commit();
        }

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        System.out.print("Entered long press-1");
        if (v.getId()== R.id.ProjectList) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            menu.setHeaderTitle(projectsList().get(((AdapterView.AdapterContextMenuInfo) menuInfo).position));
            String[] menuItems = getResources().getStringArray(R.array.menu);
            for (int i = 0; i<menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        System.out.print("Entered long press-2");
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = getResources().getStringArray(R.array.menu);
        String menuItemName = menuItems[menuItemIndex];
        String listItemName = projectsList().get(info.position);
        if(menuItemName.equals(getString(R.string.delete))){
            deleteProject(listItemName);
        }
        else if(menuItemName.equals(getString(R.string.long_press_duplicate_proj))){
            duplicateProject(listItemName);
        }
        else{
            Toast.makeText(NavigationDrawer.this, "Something is wrong", Toast.LENGTH_SHORT).show();
        }
//        Toast.makeText(Projects.this, menuItemName+" + "+listItemName, Toast.LENGTH_SHORT).show();
        return true;
    }

    /**
     * code for creating a copy of the project. Renaming of the project also implemented in the same
     * @param pname new project name
     */
    public void duplicateProject(final String pname){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.enterName);

        final EditText input = new EditText(this);

        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton(getString(R.string.OkButton), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Pattern pattern1 = Pattern.compile("\\s");
                Pattern pattern2 = Pattern.compile("\\.");
                // Pattern pattern3 = Pattern.compile("");
                String enteredProjectName=input.getText().toString();
                //Matcher matcher1 = pattern1.matcher(input.getText().toString());
                Matcher matcher2 = pattern2.matcher(enteredProjectName);
                // Matcher matcher3 = pattern3.matcher(input.getText().toString());

                //boolean found1 = matcher1.find();
                boolean found1 = false;
                boolean found2 = matcher2.find();
                boolean found3 = enteredProjectName.contains("/");
                // boolean found3 = matcher3.find();

                if (enteredProjectName.charAt(0) == ' ' || enteredProjectName.charAt(enteredProjectName.length() - 1) == ' ')
                    found1 = true;

                if (found1)
                    Toast.makeText(NavigationDrawer.this, getString(R.string.projectNameSpace), Toast.LENGTH_LONG).show();
                else if (found2)
                    Toast.makeText(NavigationDrawer.this, getString(R.string.projectNameDot), Toast.LENGTH_LONG).show();
                else if (found3)
                    Toast.makeText(NavigationDrawer.this, "Project name cannot contain '/'", Toast.LENGTH_LONG).show();
                else {
                    if (input.getText().toString().equals("")) {
                        Toast.makeText(NavigationDrawer.this, getString(R.string.projectNameEmpty), Toast.LENGTH_LONG).show();
                    }
                    else if(foundInProjectList(enteredProjectName)){
                        Toast.makeText(NavigationDrawer.this, getString(R.string.projectExists), Toast.LENGTH_LONG).show();
                    }
                    else {
                        copyDuplicateProject(pname,enteredProjectName);
                        recreate();
                    }
                }
            }
        });
        builder.setNegativeButton(getString(R.string.CancelButton), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }
    public Activity getThisActivity() {
        return this;
    }

    public List<String> projectsList() {
        Context context = getApplicationContext();
        Projectfile f = new Projectfile(context);
        List<String> myStringArray = f.DisplayProject();
        return myStringArray;
    }


    public void copyDuplicateProject(String pname, String newName){
        Projectfile f = new Projectfile(getApplicationContext());
        List<String> projects = f.AddNewProject(newName);
        try {
            f.duplicateContents(pname,newName,getThisActivity(),this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ProjectsListView(projects);
    }
    boolean foundInProjectList(String project) {
        List<String> projectList = projectsList();
        for (String str : projectList) {
            if (str.equalsIgnoreCase(project))
                return true;
        }
        return false;
    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
