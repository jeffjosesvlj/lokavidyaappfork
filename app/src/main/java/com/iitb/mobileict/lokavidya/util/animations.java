package com.iitb.mobileict.lokavidya.util;

import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.animation.Animation;
import android.widget.Button;

import com.iitb.mobileict.lokavidya.ui.Projects;

/**
 * Created by sanket on 16/12/15.
 */
public class animations {

    public static void animateFAB(Boolean isFabOpen,FloatingActionButton fab,FloatingActionButton fab1, FloatingActionButton fab2,Animation rotate_forward,Animation rotate_backward, Animation fab_open, Animation fab_close,Button fabAddButton, Button fabImportButton){



        if(Projects.isFabOpen){

            fab.startAnimation(rotate_backward);
            fab1.startAnimation(fab_close);
            fab2.startAnimation(fab_close);
           // fab3.startAnimation(fab_close);
            fab1.setClickable(false);
            fab2.setClickable(false);
           // fab3.setClickable(false);

            fabAddButton.startAnimation(fab_close);
            fabImportButton.startAnimation(fab_close);
           // fabCameraButton.startAnimation(fab_close);
            fabAddButton.setClickable(false);
            fabImportButton.setClickable(false);
           // fabCameraButton.setClickable(false);
            Projects.isFabOpen = false;
            Log.d("FAB", "close");

        } else {

            fab.startAnimation(rotate_forward);
            fab1.startAnimation(fab_open);
            fab2.startAnimation(fab_open);
           // fab3.startAnimation(fab_open);
            fab1.setClickable(true);
            fab2.setClickable(true);
           // fab3.setClickable(true);
            fabAddButton.startAnimation(fab_open);
            fabImportButton.startAnimation(fab_open);
           // fabCameraButton.startAnimation(fab_open);
            fabAddButton.setClickable(true);
            fabImportButton.setClickable(true);
           // fabCameraButton.setClickable(true);
            Projects.isFabOpen = true;
            Log.d("FAB","open");

        }
    }
    public static void animateFAB(Boolean isFabOpen,FloatingActionButton fab,FloatingActionButton fab1, FloatingActionButton fab2,FloatingActionButton fab3,Animation rotate_forward,Animation rotate_backward, Animation fab_open, Animation fab_close,Button fabAddButton, Button fabImportButton, Button fabCameraButton){



        if(Projects.isFabOpen){

            fab.startAnimation(rotate_backward);
            fab1.startAnimation(fab_close);
            fab2.startAnimation(fab_close);
            fab3.startAnimation(fab_close);
            fab1.setClickable(false);
            fab2.setClickable(false);
            fab3.setClickable(false);

            fabAddButton.startAnimation(fab_close);
            fabImportButton.startAnimation(fab_close);
            fabCameraButton.startAnimation(fab_close);
            fabAddButton.setClickable(false);
            fabImportButton.setClickable(false);
            fabCameraButton.setClickable(false);
            Projects.isFabOpen = false;
            Log.d("FAB", "close");

        } else {

            fab.startAnimation(rotate_forward);
            fab1.startAnimation(fab_open);
            fab2.startAnimation(fab_open);
            fab3.startAnimation(fab_open);
            fab1.setClickable(true);
            fab2.setClickable(true);
            fab3.setClickable(true);
            fabAddButton.startAnimation(fab_open);
            fabImportButton.startAnimation(fab_open);
            fabCameraButton.startAnimation(fab_open);
            fabAddButton.setClickable(true);
            fabImportButton.setClickable(true);
            fabCameraButton.setClickable(true);
            Projects.isFabOpen = true;
            Log.d("FAB","open");

        }
    }
}
