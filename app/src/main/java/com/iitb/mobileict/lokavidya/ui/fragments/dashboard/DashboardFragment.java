package com.iitb.mobileict.lokavidya.ui.fragments.dashboard;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.iitb.mobileict.lokavidya.Projectfile;
import com.iitb.mobileict.lokavidya.R;
import com.iitb.mobileict.lokavidya.Share;
import com.iitb.mobileict.lokavidya.ui.EditProject;
import com.iitb.mobileict.lokavidya.util.animations;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 This class handles calls to the Dashboard Fragment.
 **/
public class DashboardFragment extends Fragment implements View.OnClickListener{

    String loktemp = Environment.getExternalStorageDirectory().getAbsolutePath() + "/loktemp/" ;
    private String importProjectName;
    private String seedpath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/lokavidya/";
    public static Boolean isFabOpen = false;
    private Animation fab_open,fab_close,rotate_forward,rotate_backward;

    private Button fabAddButton,fabImportButton;
    private FloatingActionButton fabadd,fabmain,fabimport;
    List<String> items;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        getActivity().setTitle(R.string.title_dashboard);


        Projectfile pf = new Projectfile(getContext());
        items = pf.DisplayProject();

        if (!new File(seedpath + "biogas-st-marathi"+"/").exists()) {
            copyAssets();
            try {
                ZipFile seedzip = new ZipFile(loktemp+ "biogas-st-marathi.zip");
                if (!new File(seedpath).isDirectory()) {
                    File f1 = new File(seedpath);
                    f1.mkdir();
                }
                seedzip.extractAll(seedpath);
            } catch (ZipException e) {
                e.printStackTrace();
            }

            File delTemp = new File(loktemp + "biogas-st-marathi.zip");
            delTemp.delete();
            delTemp.getParentFile().delete();
        }
//-------------------------------------------------------------------------------------------------------------------------

        /*the following code loads all the folders inside the lokavidya folder and removes zips */
        importProjectName = "";
        Context context;
        context =getActivity();
        //TODO check for permissions here
        Projectfile f = new Projectfile(context);
        List<String> projectsList = f.DisplayProject_with_zips();


        for (int i = 0; i < projectsList.size(); i++) {
            System.out.println("--------------projects : " + projectsList.get(i));

            if(new File(seedpath+projectsList.get(i)+File.separator).isDirectory()) {
                File tmp_images = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/lokavidya/" + projectsList.get(i) + "/tmp_images");
                if (!tmp_images.exists()) {
                    Log.i("projectlist", "tmp_images not found, making dir");
                    tmp_images.mkdir();
                    Log.i("tmp_images", "tmp_images folder created");
                    Share.tmp_images_make(projectsList.get(i));
                }else if(tmp_images.list().length == 0){
                    Log.i("tmp_images","tmp_images is empty");
                    Share.tmp_images_make(projectsList.get(i));
                }
            }

            if (projectsList.get(i).length() < 4) continue;
            if (projectsList.get(i).substring(projectsList.get(i).length() - 4).equals(".zip")) {
                System.out.println("-" + projectsList.get(i).substring(projectsList.get(i).length() - 4) + ":inside");
                File delete_file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/lokavidya/" + projectsList.get(i));
                delete_file.delete();

            }

        }
        ListView list = (ListView)rootView.findViewById(R.id.list);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,projectsList);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
    public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
        String item = (String) adapter.getItemAtPosition(position);
        Intent intent = new Intent(getContext(), EditProject.class);
        intent.putExtra("projectname", item);
        startActivity(intent);
    }
        });


        fabadd= (FloatingActionButton) rootView.findViewById(R.id.fab_add);
        fabimport= (FloatingActionButton) rootView.findViewById(R.id.fab_import);
        fabmain = (FloatingActionButton) rootView.findViewById(R.id.fab_main);


        fabmain.setOnClickListener(this);
        fabadd.setOnClickListener(this);
        fabimport.setOnClickListener(this);

        fabAddButton=(Button)rootView.findViewById(R.id.fabAddbutton);
        fabImportButton=(Button)rootView.findViewById(R.id.fabImportButton);

        fab_open = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(rootView.getContext(),R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(rootView.getContext(),R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(rootView.getContext(),R.anim.rotate_backward);



        return rootView;
    }

    private void copyAssets() {
        AssetManager assetManager = getContext().getAssets();
        String[] files = null;
        /*try {
            files = assetManager.list("");
        } catch (IOException e) {
            Log.e("tag", "Failed to get asset file list.", e);
        }*/
        // for(String filename : files) {
        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open("biogas-st-marathi.zip");


            String out1 = Environment.getExternalStorageDirectory().getAbsolutePath() + "/loktemp/";
            if (!new File(out1).isDirectory()) {
                File f1 = new File(out1);
                f1.mkdir();
            }
            File outFile = new File(out1 + "biogas-st-marathi.zip");
            Log.i("output file", outFile.toString());


            out = new FileOutputStream(outFile);
            copyFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (IOException e) {
            Log.e("tag", "Failed to copy asset file: " + "testseedproject", e);
            e.printStackTrace();
        }
        //}
    }
    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[5120];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View v) {


        int id = v.getId();
        switch (id){
            case R.id.fab_main:

                animations.animateFAB(isFabOpen,fabmain,fabadd,fabimport,rotate_forward,rotate_backward,fab_open,fab_close,fabAddButton,fabImportButton);
                break;
            case R.id.fab_add:

                Log.d("FAB", "ADD");
                addProjectCallBack(v);
                animations.animateFAB(isFabOpen, fabmain, fabadd, fabimport, rotate_forward, rotate_backward, fab_open, fab_close, fabAddButton, fabImportButton);

                break;
            case R.id.fab_import:

                Log.d("FAB", "Import");
                importProjectCallback(v);
                animations.animateFAB(isFabOpen, fabmain, fabadd, fabimport, rotate_forward, rotate_backward, fab_open, fab_close, fabAddButton, fabImportButton);

                break;
        }




    }
    public static final int FILE_SELECT_CODE = 102;
    public void importProjectCallback(View v) {
        importProjectName = "";
        try {
            //            //************************************************************************************************
            //            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            //            builder.setTitle(R.string.enterName);
            //            final EditText input = new EditText(this);
            //            input.setInputType(InputType.TYPE_CLASS_TEXT);
            //            builder.setView(input);
            //            builder.setPositiveButton(getString(R.string.OkButton), new DialogInterface.OnClickListener() {
            //                @Override
            //                public void onClick(DialogInterface dialog, int which) {
            //                    Pattern pattern1 = Pattern.compile("\\s");
            //                    Pattern pattern2 = Pattern.compile("\\.");
            //                    // Pattern pattern3 = Pattern.compile("");
            //
            //                    Matcher matcher1 = pattern1.matcher(input.getText().toString());
            //                    Matcher matcher2 = pattern2.matcher(input.getText().toString());
            //                    // Matcher matcher3 = pattern3.matcher(input.getText().toString());
            //
            //                    //boolean found1 = matcher1.find();
            //                    boolean found1 = false;
            //                    boolean found2 = matcher2.find();
            //                    // boolean found3 = matcher3.find();
            //
            //                    if(input.getText().toString().charAt(0) == ' ' || input.getText().toString().charAt(input.getText().toString().length() -1) == ' ' )
            //                        found1 = true;
            //
            //                    if (found1)
            //                        Toast.makeText(Projects.this, getString(R.string.projectNameSpace), Toast.LENGTH_LONG).show();
            //                    else if (found2)
            //                        Toast.makeText(Projects.this, getString(R.string.projectNameDot), Toast.LENGTH_LONG).show();
            //                    else {
            //                        if (input.getText().toString().equals("")) {
            //                            Toast.makeText(Projects.this, getString(R.string.projectNameEmpty), Toast.LENGTH_LONG).show();
            //                        } else {
            //                            if(foundInProjectList(input.getText().toString())){
            //                                Toast.makeText(Projects.this, getString(R.string.projectExists), Toast.LENGTH_LONG).show();
            //                            }
            //                            else{
            //                                importProjectName = input.getText().toString();
            //                                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            //                                i.setType("*/*");
            //                                i.addCategory(Intent.CATEGORY_OPENABLE);
            //                                startActivityForResult(
            //                                        Intent.createChooser(i, getString(R.string.selectProjectToImport)), FILE_SELECT_CODE
            //                                );
            //                            }
            //                        }
            //                    }
            //                }
            //            });
            //            builder.setNegativeButton(getString(R.string.CancelButton), new DialogInterface.OnClickListener() {
            //                @Override
            //                public void onClick(DialogInterface dialog, int which) {
            //                    dialog.cancel();
            //                }
            //            });
            //            builder.show();
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.setType("application/zip");
            i.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(
                    Intent.createChooser(i, getString(R.string.selectProjectToImport)), FILE_SELECT_CODE
            );
            //************************************************************************************************
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getContext(), getString(R.string.NoFileManager), Toast.LENGTH_SHORT).show();
        }
    }

    public void addProjectCallBack(View v) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.enterName);

        final EditText input = new EditText(getActivity());

        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton(getString(R.string.OkButton), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //all the checks and validations for an appropriate project names are performed:

                Pattern pattern1 = Pattern.compile("\\s");
                Pattern pattern2 = Pattern.compile("\\.");
                // Pattern pattern3 = Pattern.compile("");

                //Matcher matcher1 = pattern1.matcher(input.getText().toString());
                Matcher matcher2 = pattern2.matcher(input.getText().toString());
                // Matcher matcher3 = pattern3.matcher(input.getText().toString());

                //boolean found1 = matcher1.find();
                boolean found1 = false;
                boolean found2 = matcher2.find();
                boolean found3 = input.getText().toString().contains("/");
                // boolean found3 = matcher3.find();

                if (input.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), getString(R.string.projectNameEmpty), Toast.LENGTH_LONG).show();
                }
                else if (input.getText().toString().charAt(0) == ' ' || input.getText().toString().endsWith(" "))
                    found1 = true;
                if (found1)
                    Toast.makeText(getActivity(), getString(R.string.projectNameSpace), Toast.LENGTH_LONG).show();
                else if (found2)
                    Toast.makeText(getActivity(), getString(R.string.projectNameDot), Toast.LENGTH_LONG).show();
                else if (found3)
                    Toast.makeText(getActivity(), "Project name cannot contain '/'", Toast.LENGTH_LONG).show();
                else {

                    //................................
                   addProject(input.getText().toString());


                }
            }
        });
        builder.setNegativeButton(getString(R.string.CancelButton), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }
    public void addProject(String newproject) {
        if (newproject.equals("") || newproject.equals(" "))
            return; //(Sanket P) changed newproject == "" to newproject.equals("").
        Projectfile f = new Projectfile(getContext());
        List<String> projects = f.AddNewProject(newproject);
        ProjectsListView(projects);
    }
    public void ProjectsListView(List<String> myStringArray) {

        ListView list = (ListView)getView().findViewById(R.id.list);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,myStringArray);
        list.setAdapter(adapter);

    }

}
