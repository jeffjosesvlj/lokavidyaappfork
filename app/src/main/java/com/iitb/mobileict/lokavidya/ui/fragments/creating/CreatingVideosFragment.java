package com.iitb.mobileict.lokavidya.ui.fragments.creating;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.iitb.mobileict.lokavidya.Projectfile;
import com.iitb.mobileict.lokavidya.R;

import java.util.List;

/**
 This class handles calls to the Create Videos Fragment.
 **/
public class CreatingVideosFragment extends Fragment {
    //Handle all the variable declarations here
    FloatingActionButton fabForCreateVideos;
    View rootView;
    ListView listViewCreateVideos;
    String[] projectName;
    String[] projectLanguage = {
            "English",
            "Hindi",
            "German"
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_create_videos, container, false);
        getActivity().setTitle(R.string.title_creating);

        //-----------------------------adding real logic from old files--------------------------------

         /*shared preferences used to store the project data*/
        SharedPreferences sharedPref;
        SharedPreferences.Editor editor;
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        editor = sharedPref.edit();
        editor.putInt("savedView", 0);
        editor.commit();

        //show all the projects in the list
        projectName= new String[displayProjects().size()];
        projectName= (String[]) displayProjects().toArray(projectName);
        //ListView listView = (ListView) findViewById(R.id.ProjectList);
        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.listitem, projectsList());
        //listView.setAdapter(adapter);
        /*registerForContextMenu(listView); //for floating context menu (on long click)
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
                String item = (String) adapter.getItemAtPosition(position);
                Intent intent = new Intent(getContext(), EditProject.class);
                intent.putExtra("projectname", item);
                startActivity(intent);
            }
        });*/

        //---------------------------------------------------------------------------------------------
        //Code for ListView
        listViewCreateVideos = (ListView) rootView.findViewById(R.id.listView_create_projects);
        CreatingVideosAdapterActivity adapterActivity = new CreatingVideosAdapterActivity(getContext(), projectName, projectLanguage);
        listViewCreateVideos.setAdapter(adapterActivity);
        registerForContextMenu(listViewCreateVideos);
        listViewCreateVideos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getActivity(), "Opening " + projectName[+position] + "...", Toast.LENGTH_LONG).show();
            }
        });
        listViewCreateVideos.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                return false;
            }
        });

        fabForCreateVideos = (FloatingActionButton) rootView.findViewById(R.id.fab_create_project);
        fabForCreateVideos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    public List displayProjects() {
        Context context = getContext();
        Projectfile f = new Projectfile(context);
        List<String> myStringArray = f.DisplayProject();
       // ProjectsListView(myStringArray);
        return  myStringArray;
    }
    public List<String> projectsList() {
        Context context = getContext();
        Projectfile f = new Projectfile(context);
        List<String> myStringArray = f.DisplayProject();
        return myStringArray;
    }


}
